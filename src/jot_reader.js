import {eachLine} from './line-reader.js'
import stream from 'stream'

const section_delimiter = "!#:----------:#!";
const special_line = "=======================================================";
const joy_url = "www.joyoftournaments.com";

// promisifyish
var eachLineRead = function(filename, /*options,*/ iteratee) {
  return new Promise(function(resolve, reject) {
    eachLine(filename, /*options,*/ iteratee, function(err) {
      if (err) {
        console.log("eachLineRead error", err)
        reject(err)
      } else {
        // console.log("eachLineRead resolve")
        resolve()
      }
    })
  })
}

// eachLineRead('file.txt', function(line) {
//   console.log(line);
// }).then(function() {
//   console.log('done');
// }).catch(function(err) {
//   console.error(err);
// });

function add_value(section, data, string, index, headers) {
  if (headers) {
    section._headers[index] = string;
  } else {
    data[section._headers[index]] = string;
  }
}

export async function read(data, progress) {
  let current_section = null;
  let new_section = false;
  var headers = false;
  var index = 0;
  let sections = {};
  
  const jot = {
    sections: sections,

    getEntry: function(id) {
      return this.sections.entrys[id];
    },
    getEventRound: function(id) {
      return this.sections.evrd[id];
    },
    getEvent: function(id) {
      return this.sections.events[id];
    },
    getJudgeSection: function(id) {
      return this.sections.judsects[id];
    },
    getJudgeSectionEntry: function(id) {
      return this.sections.jse[id];
    },
    getName: function(id) {
      return this.sections.names[id];
    },
    getRoom: function(id) {
      return this.sections.rooms[id];
    },
    getSchool: function(id) {
      return this.sections.schools[id];
    },
    getSection: function(id) {
      return this.sections.sections[id];
    },
    getSectionEntry: function(id) {
      return this.sections.sectents[id];
    },
    getTeam: function(id) {
      return this.sections.teams[id];
    },


    getEntries: function() {
      return this.sections.entrys;
    },
    getEventRounds: function() {
      // console.log(Object.keys(this.sections))
      return this.sections.evrd;
    },
    getEvents: function() {
      return this.sections.events;
    },
    getJudgeSectionEntrys: function() {
      return this.sections.jse;
    },
    getJudgeSections: function() {
      return this.sections.judsects;
    },
    getNames: function() {
      return this.sections.names;
    },
    getRooms: function() {
      return this.sections.rooms;
    },
    getSchools: function() {
      return this.sections.schools;
    },
    getSections: function() {
      return this.sections.sections;
    },
    getSectionEntrys: function() {
      return this.sections.sectents;
    },
    getTeams: function() {
      return this.sections.teams;
    },

  }

  const ss = new stream.Readable();
  ss._read = function noop() {}; // redundant? see update below
  ss.push(data);
  ss.push(null);
  try {
    await eachLineRead(ss, function(line, last/*, cb*/) {
      var data = {};
      if (last) {
        return false
      }
      if (new_section) {
        // console.log("new section", line)
        new_section = false;
        headers = true;
        let decoded = line.split(":");
        if (decoded.length === 2) {
          // console.log("section", line);
          current_section = {
            id: decoded[0],
            _headers: []
          };
          sections[decoded[1]] = current_section;
        } else {
          current_section = null;
        }
      } else if (line === section_delimiter) {
        new_section = true;
        // console.log("section delim detected")
      } else if (line !== special_line && line !== joy_url && line.length !== 0) {
        let i = 0;
        let nextChar = line.charAt(0);
        let quoting = false;
        let string = "";
        // console.log(line);
        // if (headers) {
        //   console.log(">>>>>>>> headers");
        // }
        while (i < line.length) {
          let char = nextChar ? nextChar : line.charAt(i);
          nextChar = line.charAt(i + 1);
          // console.log("==", char, nextChar);
          if (char === '"') {
            if (quoting) {
              // console.log("end quote");
              quoting = false;
            } else {
              // console.log("beg quote");
              quoting = true;
            }
          } else if (quoting) {
            string += char;
          } else if (char === ",") {
            // console.log("found --" + string + "--");
            // console.log(string.trim());
            // next value in `string`
            add_value(current_section, data, string.trim(), index++, headers);
            string = "";
          } else {
            string += char;
          }
          i++;
        }
      if (current_section) {
        // console.log("found last --" + string + "--");
        // console.log(string.trim());
        // console.log("----------------------");
        // last value in `string`
        add_value(current_section, data, string.trim(), index, headers);
        // console.log(data);
        // console.log("----------------------");
        if (progress) {
          progress(current_section.id)
        }
        switch (current_section.id) {
          case "01": // events
            // [ 'Event ID', 'Name', 'Attributes', 'Entry Fee', 'Pattern', 'Duration', 'Copies', 'Complete', 'Method', 'Drop Fee', 'Team Count', 'Entry Blank', 'Drop Fee 2', 'Drop Fee 3', 'Drop Fee 4', 'Speaker Goal', 'Flags', 'Min Points', 'Max Points', 'NFL', 'Tab Method', 'Rows', 'Columns', 'Web Event ID', 'Performance', 'Web Type', 'Ballot Style', 'Type', 'Prelim Method' ]
            data.Event_ID = data['Event ID'];
            // console.log("events", data);
            if (data.Event_ID) {
              current_section[data.Event_ID] = data;
            }
            break;
          case "02": // actions
            // ignore
            break;
          case "03": // bag
            // ignore
            break;
          case "04": // entrys
             // [ 'Entry ID', 'School ID', 'Event ID', 'Squad Seq', 'Name ID', 'Flags', 'Code', 'User Flags', 'Web Entry ID', 'Seed' ] 
            data.Entry_ID = data['Entry ID'];
            data.School_ID = data['School ID'];
            data.Event_ID = data['Event ID'];
            data.Name_ID = data['Name ID'];
            // console.log("entrys", data);
            if (data.Entry_ID) {
              current_section[data.Entry_ID] = data;
            }
            break;
          case "05": // evrd
            // Event ID, Round ID, Sections, Category, er_seq, Date, Time, Draw, Flip, Panel, Flight, Panel Size, Auto Flight, Attr, User Flags
            data.Event_ID = data['Event ID']
            data.Round_ID = data['Round ID']
            data.Panel_Size = data['Panel Size']
            if (data.Event_ID && data.Round_ID) {
              current_section[data.Event_ID + ':' + data.Round_ID] = data  // make round id unique.  will need to be able to iterate/sort
            }
            break;
          case "06": // fees
            // ignore
            break;
          case "07": // jse
             // Judge ID, Section ID, Entry ID, Points, Rank, Points1, Points2
            data.Section_ID = data['Section ID'];
            data.Entry_ID = data['Entry ID'];
            // console.log("jse", data);
            if (data.Section_ID) {
              if (current_section[data.Section_ID]) {
                current_section[data.Section_ID].push(data);
              } else {
                current_section[data.Section_ID] = [data];
              }
            }
            break;
          case "08": // judcommit
            // ignore
            break;
          case "09": // judges
            // ignore
            break;
          case "10": // judprefs
            // ignore
            break;
          case "11": // judsects
            // Judge ID, Section ID, JS Seq
            data.Judge_ID = data['Judge ID']
            data.Section_ID = data['Section ID']
            if (data.Section_ID) {
              if (current_section[data.Section_ID]) {
                current_section[data.Section_ID].count++
              } else {
                current_section[data.Section_ID] = {count: 1}
              }
            }
            break;
          case "12": // names
            // [ 'Name ID', 'School ID', 'Name', 'Code', 'Web Student ID' ]
            data.Name_ID = data['Name ID'];
            data.School_ID = data['School ID'];
            // console.log("names", data);
            if (data.Name_ID) {
              current_section[data.Name_ID] = data;
            }
            break;
          case "13": // pairopts
            // ignore
            break;
          case "14": // restrictions
            // ignore
            break;
          case "15": // rooms
            // Room ID, Room, Hall, Notes, Flags, User Flags
            // 104,"Admin 100","Administration","Classroom 30 max",112,0
            data.Room_ID = data['Room ID'];
            data.Room_Name = data['Room'];
            data.Room_Hall = data['Hall'];
            data.Room_Notes = data['Notes'];
            if (data.Room_ID) {
              current_section[data.Room_ID] = data;
            }
            // console.log("data.Room*", data)
            break;
          case "16": // rsvps
            // ignore
            break;
          case "17": // schools
            // [ 'School ID', 'Code', 'Name', 'Coach', 'Squad Count', 'Arrived', 'Division', 'City', 'Phone', 'Fax', 'Coach Phone', 'notes', 'status', 'addr1', 'addr2', 'state', 'zip', 'email', 'flags', 'District', 'Region', 'Other Coaches', 'Trophy Points', 'Web School ID', 'Cell Phone', 'Alt Email' ]
            data.School_ID = data['School ID'];
            // console.log("schools", data);
            if (data.School_ID) {
              current_section[data.School_ID] = data;
            }
            break;
          case "18": // sections
            data.Section_ID = data['Section ID'];
            data.Event_ID = data['Event ID'];
            data.Round_ID = data['Round ID'];
            data.Room_ID = data['Room ID'];
            data.ER_Seq = data['ER Seq'];
            data.Num_Judges = data['Num Judges'];
            // console.log("sections", data);
            if (data.Section_ID) {
              current_section[data.Section_ID] = data;
            }
            break;
          case "19": // sectents
            data.Section_ID = data['Section ID'];
            data.Entry_ID = data['Entry ID'];
            data.Speaker_Order = data['Speaker Order'];
            data.Speaker_Actual = data['Speaker Actual'];
            // console.log("sectents", data);
            if (data.Section_ID) {
              if (current_section[data.Section_ID]) {
                current_section[data.Section_ID].push(data);
              } else {
                current_section[data.Section_ID] = [data];
              }
            }
            break;
          case "20": // seeds
            // ignore
            break;
          case "21": // selections
            // ignore
            break;
          case "22": // seqstrs
            // ignore
            break;
          case "23": // sweeps
            // ignore
            break;
          case "24": // teams (relation)
            // [ 'Entry ID', 'Name ID'+ ]
            let Entry_ID = data["Entry ID"];
            let Name_ID = data["Name ID"];
            // console.log("teams", data);
            if (Entry_ID) {
              if (current_section[Entry_ID]) {
                current_section[Entry_ID].push(Name_ID);
              } else {
                current_section[Entry_ID] = [Name_ID];
              }
            }
            break;
          case "25": // team4
            // ignore
            break;
          case "26": // todos
            // ignore
            break;
          case "27": // roomavails
            // ignore
            break;
          case "28": // TRN
            // ignore
            break;
          case "31": // outcomes
            // ignore
            break;
          default:
            console.log("unexpected value", current_section.id)
            // ignore
          }
        }
      headers = false;
      index = 0;
      }
    })
    // console.log("returning", jot)
    return jot
  } catch (e) {
    console.error(e)
    return null
  }
}
