import {read} from './jot_reader.js'
import fs from 'fs'

function findEventID(jot, event_name) {
  const events = jot.getEvents()
  // console.log("events", events)
  for (let key of Object.keys(events)) {
    const event = events[key]
    if (event['Entry Blank'] === event_name) {
      return event.Event_ID
    }
  }
  return null
}

function analyze(jot, event_id, round_id) {
  const rounds = jot.getEventRounds()
  const teams = jot.getTeams()
  const entries = jot.getEntries()
  const sections = jot.getSections()
  const sectents = jot.getSectionEntrys()

  const jot_key = `${event_id}:${round_id}`
  const round = jot.getEventRound(jot_key)
  // console.log("round", round)

  const event = jot.getEvent(event_id)
  // console.log("event", event)

  const round_data = {
    round_id: jot_key,  // this is "Event_ID:Round_ID"
    // panel_size: round.Panel_Size,
    seq: round.er_seq,

    panel_size: (round.Panel === "1") ? round.Panel_Size : "1",
    panel: round.Panel,

    date: round.Date,
    time: round.Time,
    flip: round.Flip,
    flight: round.Flight,
    sections: [],
  }

  // console.log("round_data", round_data)

  const event_name = event.Name
  // console.log("round", event_name, "Round:", round.Round_ID, "Flight:", round.Flight, "Sections:", round.Sections, "seq:", round.er_seq, "Panel Size:", round.Panel_Size, "Date:", round.Date, "Time:", round.Time)

  // TODO: do we have sections?
  const rooms = []
  Object.keys(sections).forEach(function(key) {
    if (key === "id" || key === "_headers") return
    const section = sections[key]
    // console.log(section.Event_ID, section.Round_ID)
    if (jot_key === (section.Event_ID + ':' + section.Round_ID)) {
      // if (section.Room_ID !== -1 && section.Room_ID !== "-1") {
      //   rooms.push(jot.getRoom(section.Room_ID))
      // }
      if (section.Room_ID !== -1 && section.Room_ID !== "-1") {
        const id = parseInt(section.Room_ID)
        const seq = parseInt(section.ER_Seq)
        const room = jot.getRoom(id)
        // console.log()
        rooms[seq - 1] = room
      }
    }
  })
  // console.log("***---------------rooms", rooms)

  Object.keys(sections).forEach(function(key) {
    if (key === "id" || key === "_headers") return
    const section = sections[key]
    // console.log(jot_key, section.Event_ID, section.Round_ID)
    if (jot_key === (section.Event_ID + ':' + section.Round_ID)) {

      const event = jot.getEvent(section.Event_ID)
      // console.log("event during import", event)

      const seq = section['ER Seq']
      // console.log(round_data.sections)
      // console.log("seq'", seq, (parseInt(seq)/2).toFixed(0), ((parseInt(seq) + 1)/2).toFixed(0))
      let room = round.Flight === '1' ? rooms[(parseInt(seq)/2).toFixed(0) - 1] : jot.getRoom(section.Room_ID)

      if (!room) {
        // console.log("=----==================------------ something when wrong, no room")
        // console.log("seq'", seq, (parseInt(seq)/2).toFixed(0), ((parseInt(seq) + 1)/2).toFixed(0))
      }
      // if ((section.Room_ID === -1 || section.Room_ID === "-1") && round.Flight === '1') {
      //   console.log("flighting:", room, section, seq, sections_in_flight)
      // }
      const room_name = room ? room.Room_Name : "------"
      const room_id = room ? room.Room_ID : -1
      const judge_section = jot.getJudgeSection(section.Section_ID)
      const judge_section_count = judge_section ? judge_section.count : -1

      // console.log("section", section.Section_ID, "Seq:", section.ER_Seq, "Room:", room_name, "judges:", section.Num_Judges)

      const section_data ={
        room_name,
        room_id,  // TODO: assumes rooms can be matched using this
        section_id: section.Section_ID,
        seq: section.ER_Seq,
        // judges: section.Num_Judges, // TODO: seems to be always 1, ignore?
        judge_section_count,  // this appears to be correct
        entries: [],
        room,
      }
      round_data.sections.push(section_data)

      const section_entries = sectents[section.Section_ID]
      if (section_entries) {
        const count = section_entries.length


        // console.log("sectent", section.Section_ID, "Count:", count, "Round:", section.Round_ID, "Seq:", section.ER_Seq, "Judges:", section.Num_Judges, "judge_section_count:", judge_section_count, "Event:", event.Name, "Room:", room_name)
        for (let i = 0; i < count; i++) {
          const entry_ref = section_entries[i]
          // Section ID, Entry ID, Speaker Order, Speaker Actual, ID
          // console.log("entry_ref", entry_ref)
          const entry = jot.getEntry(entry_ref.Entry_ID)
          const name = entry ? jot.getName(entry.Name_ID) : undefined
          const school = entry ? jot.getSchool(entry.School_ID) : undefined
          const team = entry ? jot.getTeam(entry.Entry_ID) : undefined
          // console.log(team)
          const name1 = (team && team[0] && jot.getName(team[0])) ? jot.getName(team[0]).Name : "N/A"
          const name2 = (team && team[1] && jot.getName(team[1])) ? jot.getName(team[1]).Name : "N/A"
          // console.log(" ", i, entry ? entry.Entry_ID : "-", name ? name.Name : "-", school ? school.Code : "-", name1, name2)
          section_data.entries.push({
            entry_id: entry_ref.Entry_ID,  // assumes entries can be matched using this
            name: name ? name.Name : "-",
            school_code: school ? school.Code : "-",
            school_name: school ? school.Name : "-",
            team,
            name1,
            name2,
            speaker_order: entry_ref['Speaker Order'],
            speaker_actual: entry_ref['Speaker Actual'],
          })
        }
      } else {
        console.log("empty section entry", section.Section_ID)
      }

    }
  })
  // console.log("round_data", JSON.stringify(round_data, null, 2))
  return round_data
}


(async () => {
  const jot_file = process.argv[2]
  const event = process.argv[3]
  const round_id = process.argv[4]

  // console.log(`Extracting ${event} Round ${round_id} from ${jot_file}`)

  const content = fs.readFileSync(jot_file)
  const jot = await read(content)

  // console.log(JSON.stringify(jot, null, 2))

  const event_id = findEventID(jot, event)
  if (!event_id) {
    console.log("event", event, "not found")
    process.exit(-1)
  }
  // console.log("event_id", event_id)
  // const evrd = jot.getEventRound(`${event_id}:${round}`)
  // console.log("evrd", evrd)
  const data = analyze(jot, event_id, round_id)
  console.log(jot.getEvent(event_id).Name)
  console.log(`Round ${round_id}`)

  const sorted_sections = data.sections.sort((a, b) => a.room_name.localeCompare(b.room_name))

  for (let section of sorted_sections) {
    if (section.room_name !== '------') {
      console.log(section.room_name)
    }
  }
  console.log()
  for (let section of sorted_sections) {
    if (section.entries[0].name !== '-' && section.entries[1].name !== '-') {
      console.log(section.entries[0].school_code, section.entries[0].name)
      console.log()
      console.log(section.entries[1].school_code, section.entries[1].name)
      console.log()
    }
  }

})()

